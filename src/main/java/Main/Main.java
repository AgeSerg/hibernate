package Main;

import dao.*;
import domain.Addresses;
import domain.Contact;
import domain.Phone;
import servise.HibernateAddresses;
import servise.HibernateContact;
import servise.HibernatePhone;
import utils.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Main {


    public static void main(String[] args) throws PersistException, SQLException {
        Contact contact = new Contact();
        contact.setName("Satisfied");

        Phone phone = new Phone();
        phone.setNumber("1");
        phone.setType("fax");

        Phone secondPhone = new Phone();
        secondPhone.setNumber("333");
        secondPhone.setType("cool");

        LinkedList<Phone> phones = new LinkedList<>();
        phones.add(phone);
        phones.add(secondPhone);
        contact.setPhones(phones);



        Addresses addresses = new Addresses();
        addresses.setCity("Rome");
        addresses.setStreet("good");
        addresses.setBuilding(228);

        contact.setAddress(addresses);



        try {
            AddressesDao hibernateAddresses = new HibernateAddresses();
            ContactDao hibernateContact = new HibernateContact();
            PhoneDao hibernatePhone = new HibernatePhone();
            /*System.out.println(hibernateContact.getByPK(1));
            System.out.println(hibernateAddresses.getByPK(1));
            System.out.println(hibernatePhone.getByPK(1));*/
           /* System.out.println(hibernateAddresses.findByContactId(5));
            System.out.println(hibernatePhone.findByContactId(1));*/
            /*System.out.println();
            Contact contact1 = hibernateContact.getByPK(5);
            hibernateContact.delete(contact);*/
            hibernateContact.persist(contact);
            //hibernateAddresses.persist(addresses);
            Contact contact1 = new Contact();
            Phone phone2 = new Phone();
            phone2.setType("home");
            phone2.setNumber("23445545");
            contact.getPhones().add(phone2);
            hibernateContact.update(contact);















            //hibernateContact.delete(contact);
            //hibernateContact.update(contact);
            //hibernateAddresses.update(addresses);
            //hibernatePhone.update(phone);
/*
            System.out.println(hibernateContact.getAll());
            System.out.println(hibernatePhone.getAll());
            System.out.println(hibernateAddresses.getAll());*/

        } finally {
            HibernateUtil.shutdown();
        }





        /*H2SqlDaoFactory factory = new H2SqlDaoFactory();
        try {
            connection = factory.getContext();
            GenericDao dao = factory.getDao(connection, Phone.class);
            GenericDao dao1 = factory.getDao(connection, Contact.class);
            GenericDao dao2 = factory.getDao(connection, Addresses.class);
            dao1.persist(contact);
            dao.persist(phone);
            dao2.persist(addresses);

            ArrayList<GenericDao> list = new ArrayList<>();
        } finally {
            connection.close();
        }*/

    }

}
