package domain;



import com.sun.istack.NotNull;
import dao.Identified;
import javax.persistence.*;

@Entity
@Table(name = "PHONE")
public class Phone implements Identified<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phone_id")
    private int phoneId;

    @NotNull
    @Column(unique = true, name = "number")
    private String number;

    @Column(name = "type")
    private String type;

    public Phone() {}

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Integer getId() {
        return getPhoneId();
    }

    @Override
    public String toString() {
        return "Phone{" +
                "phoneId=" + phoneId +
                ", number='" + number + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
