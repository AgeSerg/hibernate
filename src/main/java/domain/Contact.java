package domain;

import dao.Identified;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "contact")
public class Contact implements Identified<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_id")
    private Integer contactId;

    @Column(name = "name")
    private String  name;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Addresses address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn
    private List<Phone> phones;

    public Contact() {}

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Addresses getAddress() {
        return address;
    }

    public void setAddress(Addresses address) {
        this.address = address;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getId() {
        return getContactId();
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "contactId=" + contactId +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }

}
