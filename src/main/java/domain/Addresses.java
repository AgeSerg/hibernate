package domain;

import dao.Identified;
import org.h2.engine.User;

import javax.persistence.*;

@Entity
@Table(name = "ADDRESSES")
public class Addresses implements Identified<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private int addressId;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "building")
    private int building;

    @OneToOne
    @JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
    private Contact contact;

    public Addresses() {}

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setAddressId(int id) {
        addressId = id;
    }

    public int getAddressId() {
        return addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    @Override
    public Integer getId() {
        return getAddressId();
    }

    @Override
    public String toString() {
        return "Addresses{" +
                "addressId=" + addressId +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", building=" + building +
                '}';
    }

}
