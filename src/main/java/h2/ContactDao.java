package h2;

import dao.AbstractJDBCDao;
import dao.PersistException;
import domain.Contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class ContactDao extends AbstractJDBCDao<Contact, Integer> {
    private static final String SELECT_QUERY = "SELECT id, name FROM Contact";
    private static final String CREATE_QUERY = "INSERT INTO Contact(" +
                                               "name) VALUES (?)";
    private static final String UPDATE_QUERY = "UPDATE Contact SET name = ? " +
                                               "WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM Contact " +
                                               "WHERE id = ?";
    private static final String SEARCH_TERM = " where id = last_insert_id()";
    private static final String CONDITION = " WHERE id = ?";

    @Override
    public String getSearchTerm() {
        return SEARCH_TERM;
    }

    @Override
    public String getCondition() {
        return CONDITION;
    }

    @Override
    public Contact create() throws PersistException {
        Contact contact = new Contact();
        return persist(contact);
    }

    private class PersistContact extends Contact {
        public void setId(int id) {
            super.setContactId(id);
        }

    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    public ContactDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Contact> parseResultSet(ResultSet rs) throws PersistException {
        List<Contact> result = new LinkedList<>();
        try {
            while (rs.next()) {

                PersistContact contact = new PersistContact();
                contact.setId(rs.getInt("id"));
                contact.setName(rs.getString("name"));
                result.add(contact);
            }

        } catch (Exception e) {
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Contact object) throws PersistException {
        try {
            statement.setString(1, object.getName());
        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Contact object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setInt(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

}
