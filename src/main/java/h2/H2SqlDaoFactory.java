package h2;

import dao.DaoFactory;
import dao.GenericDao;
import dao.PersistException;
import domain.Addresses;
import domain.Contact;
import domain.Phone;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class H2SqlDaoFactory implements DaoFactory<Connection> {

    private static final String USER = "sergey";
    private static final String PASSWORD = "11111111";
    private static final String URL = "jdbc:h2:~/IdeaProjects/repos/phonebook/Phonebook/src/main/resources/resources";
    private static final String DRIVER = "org.h2.Driver";
    private Map<Class, DaoCreator> creators;

    public H2SqlDaoFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        creators = new HashMap<Class, DaoCreator>();
        creators.put(Addresses.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new AddressesDao(connection);
            }

        });

        creators.put(Contact.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new ContactDao(connection);
            }

        });

        creators.put(Phone.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new PhoneDao(connection);
            }

        });

    }

    public Connection getContext() throws PersistException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

        return  connection;
    }

    @Override
    public GenericDao getDao(Connection connection, Class daoClass) throws PersistException {
        DaoCreator creator = creators.get(daoClass);
        if (creator == null) {
            throw new PersistException("Dao object for " + daoClass + " not found.");
        }

        return creator.create(connection);
    }

}
