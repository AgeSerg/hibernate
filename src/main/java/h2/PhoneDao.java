package h2;

import dao.AbstractJDBCDao;
import dao.GenericDao;
import dao.PersistException;
import domain.Phone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class PhoneDao  extends AbstractJDBCDao<Phone, Integer> implements GenericDao<Phone, Integer> {
    private static final String SELECT_QUERY = "SELECT id, user_id, number, type" +
                                               " FROM PHONE";
    private static final String CREATE_QUERY = "INSERT INTO PHONE (user_id," +
                                               " number, type)" +
                                               " VALUES (?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE PHONE SET user_id = ?" +
                                               " number = ?  type = ? " +
                                               "WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM PHONE " +
                                               "WHERE PHONE_ID = ?";
    private static final String SEARCH_TERM = " where id = last_insert_id()";
    private static final String CONDITION = " WHERE id = ?";

    public PhoneDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSearchTerm() {
        return SEARCH_TERM;
    }

    @Override
    public String getCondition() {
        return CONDITION;
    }

    @Override
    public Phone create() throws PersistException {
        Phone phone = new Phone();
        return persist(phone);
    }

   /* private class PersistPhone extends Phone {
        public void setContactId(int id) {
            super.setContact(id);
        }

    }*/

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected List<Phone> parseResultSet(ResultSet rs) throws PersistException {
        List<Phone> result = new LinkedList<>();
        try {
           /* while (rs.next()) {
                PersistPhone phone = new PersistPhone();
                phone.setContactId(rs.getInt("user_id"));
                phone.setNumberId(rs.getInt("id"));
                phone.setNumber(rs.getString("number"));
                phone.setType(rs.getString("type"));
                result.add(phone);
            }*/

        } catch (Exception e) {
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Phone object) throws PersistException {
        /*try {
            statement.setInt(1, object.getContactId());
            statement.setString(2, object.getNumber());
            statement.setString(3,object.getType());
        } catch (Exception e) {
            throw new PersistException(e);
        }*/

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Phone object) throws PersistException {
       /* try {
            statement.setInt(1, object.getContactId());
            statement.setString(2, object.getNumber());
            statement.setString(3, object.getType());
            statement.setInt(4, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }*/

    }

}
