package servise;

import dao.GenericDao;
import dao.PersistException;
import dao.PhoneDao;
import domain.Phone;
import org.hibernate.Session;
import org.hibernate.query.Query;
import utils.SessionUtil;

import java.util.List;

public class HibernatePhone extends SessionUtil implements PhoneDao {
    private static final String FIND_LAST_INSERT = "from PHONE where phone_id = :phone_id";
    private static final String PK = "phone_id";
    private static final String SELECT_BY_ID = "SELECT * FROM PHONE WHERE PHONEID = :phone_id";
    private static final String SELECT_ALL = "SELECT * FROM PHONE";
    private static final String SELECT_BY_CONTACT_ID = "SELECT * FROM PHONE WHERE CONTACT_ID = :CONTACT_ID";
    private static final String CONTACT_ID = "CONTACT_ID";

    @Override
    public Phone create() throws PersistException {
        return null;
    }

    @Override
    public Phone persist(Phone object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.save(object);

       /* Query query = session.createNativeQuery(FIND_LAST_INSERT).addEntity(Phone.class);
        query.setParameter(PK, object.getNumber());
        Phone result = (Phone) query.getSingleResult();*/

        closeTransactionSesstion();

        return object;
    }

    @Override
    public Phone getByPK(Integer id) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_ID).addEntity(Phone.class);
        query.setParameter(PK, id);
        Phone phone = (Phone) query.getSingleResult();

        closeTransactionSesstion();

        return phone;
    }

    @Override
    public void update(Phone object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.update(object);

        closeTransactionSesstion();
    }

    @Override
    public void delete(Phone object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.remove(object);

        closeTransactionSesstion();

    }

    @Override
    public List<Phone> getAll() throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_ALL).addEntity(Phone.class);
        List<Phone> phones = query.list();

        closeTransactionSesstion();

        return phones;
    }

    @Override
    public List<Phone> findByContactId(int contactId) {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_CONTACT_ID).addEntity(Phone.class);
        query.setParameter(CONTACT_ID, contactId);
        List<Phone> phones = query.list();

        closeTransactionSesstion();

        return phones;
    }
}
