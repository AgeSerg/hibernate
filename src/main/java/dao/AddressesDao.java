package dao;

import domain.Addresses;
import domain.Contact;

public interface AddressesDao extends GenericDao<Addresses, Integer> {
    Addresses findByContactId(int id);
}
