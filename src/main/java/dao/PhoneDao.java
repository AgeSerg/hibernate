package dao;

import domain.Phone;

import java.util.List;

public interface PhoneDao extends GenericDao<Phone, Integer> {
    List<Phone> findByContactId(int contactId);
}
