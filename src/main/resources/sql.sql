CREATE TABLE CONTACT (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE ADDRESSES(
    user_ID int NOT NULL,
    city VARCHAR(30) NOT NULL,
    street VARCHAR(30) NOT NULL,
    building int NOT NULL,
    CONSTRAINT adresses_contact_fk
    FOREIGN KEY (user_id)  REFERENCES CONTACT (id)
);

CREATE TABLE PHONE(
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    number VARCHAR(20) NOT NULL UNIQUE,
    type VARCHAR(20),
    CONSTRAINT phone_contact_fk
    FOREIGN KEY (user_id)  REFERENCES CONTACT (id)
    ON DELETE CASCADE
);
